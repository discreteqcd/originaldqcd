
import math as m
import random as r
from math import exp, sqrt, cos, sin

import sys

from vector import Vec4
from ltmatrix import LTmatrix
from particle import Particle
from baseline import Baseline

from scales import LambdaQCD

from basics import eq, ne, le, ge

class Kinematics:

    def __init__(self, base):
        self.baseline = base
        self.usedTips = []
        return None

###############################################################################
    def createEvolvedEvent(self,event, kappaMax):

        foundBranching = True
        counter=0
        while foundBranching and counter < 1000:
          counter=counter+1
          foundBranching, splitIndex = self.getNextSplitting(kappaMax)
          if foundBranching == False: break
          for i in range(len(splitIndex)):
            kappaMax = self.baseline.tips[splitIndex[i]][3]
            self.updateKinematics(splitIndex[i],event)
            self.usedTips.append(splitIndex[i])

        self.updateEvent(event)

        pIn = event[0].mom + event[1].mom
        pOut = Vec4(0.,0.,0.,0.)
        for i in range(2,len(event)):
          pOut = pOut + event[i].mom

        if  ne(pIn.E, pOut.E) or ne(pIn.px,pOut.px) or ne(pIn.py,pOut.py) or ne(pIn.pz,pOut.pz):
          print ("Error: Momentum not conserved:")
          print (pIn)
          print (pOut)
          for p in event:
            print (p)
          sys.exit()

        return True

###############################################################################
    def findNeighbors(self,event, iSplit):

        posMin=10000000
        posMax=-1
        for p in self.baseline.tips:
          posMin = min(posMin, p[-1])
          posMax = max(posMax, p[-1])

        iPosEventIndex=0
        iFind=iSplit+1
        found=False
        while not found and iFind <= posMax:
          for ip in range(len(event)):
            if event[ip].pos == iFind and event[ip].pos != 1:
              found=True
              iPosEventIndex=ip
              break
          if not found:
            iFind=iFind+1

        iNegEventIndex=0
        iFind=iSplit-1
        found=False
        while not found and iFind >= posMin:
          for ip in range(len(event)):
            if event[ip].pos == iFind and event[ip].pos != 2:
              found=True
              iNegEventIndex=ip
              break
          if not found:
            iFind=iFind-1

        if iPosEventIndex==0:
          for ip in range(len(event)):
            if event[ip].pos == 2:
              iPosEventIndex=ip
              break

        if iNegEventIndex==0:
          for ip in range(len(event)):
            if event[ip].pos == 1:
              found=True
              iNegEventIndex=ip
              break

        if iPosEventIndex==0 or iNegEventIndex==0:
          print (posMin, iSplit, posMax)
          for p in event:
            print (p.pos, p)
          sys.exit()

        return iNegEventIndex, iPosEventIndex 

###############################################################################

    def updateKinematics(self,splitIndex,event):

        split = self.baseline.tips[splitIndex]

        iSplit = split[-1]

        ipNeg, ipPos = self.findNeighbors(event,iSplit)
        iNeg = event[ipNeg].pos
        iPos = event[ipPos].pos

        if  le ((event[ipNeg].mom+event[ipPos].mom).M2(), 0.):
          print ("Error in kinematics: sijk =", (event[ipNeg].mom+event[ipPos].mom).M2(), " < 0.0")
          return False

        lambda12 = self.baseline.shortestDistance2(iNeg,iPos)
        lambda13 = self.baseline.shortestDistance2(iNeg,iSplit)
        lambda23 = self.baseline.shortestDistance2(iSplit,iPos)

        if le(lambda12,0.) or le(lambda13,0.) or le(lambda23,0.):
          print ("Error in kinematics: Zero lambda measures ")
          sys.exit()
          return False 

        sij=LambdaQCD*LambdaQCD*m.exp(lambda13)
        sjk=LambdaQCD*LambdaQCD*m.exp(lambda23)
        sijk2=LambdaQCD*LambdaQCD*exp(lambda12)

        sijk = ((event[ipNeg].mom+event[ipPos].mom).M2())

        if sij + sjk > sijk:
          print ("Error in kinematics for event size ", len(event), ": sij+sjk=",sij+sjk, "> sijk=",sijk, sijk2)
          print ("iNeg=", iNeg, " iSplit=", iSplit, " iPos=", iPos )
          for p in event:
            print (p.pos, p.pid, p.mom)
          return False

        mI=0.
        mK=0.
        mi=0.
        mj=0.
        mk=0.
        success, postBranchingMomenta = self.branch(mI,mK,mi,mj,mk,sijk,sij-mi*mi-mj*mj,sjk-mj*mj-mk*mk)

        if not success: return False

        fromCM = LTmatrix()
        fromCM.FromCMframe(event[ipNeg].mom,event[ipPos].mom)
        pi = fromCM*postBranchingMomenta[0]
        pj = fromCM*postBranchingMomenta[1]
        pk = fromCM*postBranchingMomenta[2]

        event.append(Particle(21,pj,[0,0]))
        event[-1].SetPos(split[-1])
        event[ipNeg].mom = pi
        event[ipPos].mom = pk

        return True

    def branch(self,mI,mK,mi,mj,mk,sijk,sij,sjk):

       # Convert to names used in Vincia
       m2I=mI*mI
       m2K=mK*mK
       m2Ant=sijk
       mAnt=sqrt(m2Ant)
       sAnt = m2Ant - m2I - m2K;

       mass0 = mi
       mass1 = mj
       mass2 = mk

       s01 = sij
       s12 = sjk
       s02 = m2Ant - s01 - s12 - mass0*mass0 - mass1*mass1 - mass2*mass2;


       E0 = (mass0*mass0 + s01/2 + s02/2)/mAnt;
       E1 = (mass1*mass1 + s12/2 + s01/2)/mAnt;
       E2 = (mass2*mass2 + s02/2 + s12/2)/mAnt;

       ap0 = sqrt( E0*E0 - mass0*mass0 );
       ap1 = sqrt( E1*E1 - mass1*mass1 );
       ap2 = sqrt( E2*E2 - mass2*mass2 );

       cos01 = (E0*E1 - s01/2)/(ap0*ap1);
       cos02 = (E0*E2 - s02/2)/(ap0*ap2);

       if ge(cos01,1.):
         cos01=1.0
       elif le(cos01,-1.):
         cos01=-1.0

       if ge(cos02,1.):
         cos02=1.0
       elif le(cos02,-1.):
         cos02=-1.0

       sin01 = sqrt(max(0.,1.-cos01*cos01))
       sin02 = sqrt(max(0.,1.-cos02*cos02))

       p1cm = Vec4(E0,0.0,0.0,ap0)
       p2cm = Vec4(E1,-ap1*sin01,0.0,ap1*cos01);
       p3cm = Vec4(E2,ap2*sin02,0.0,ap2*cos02);

       sig2 = m2Ant + m2I - m2K;
       sAntMin = 2*sqrt(m2I*m2K);
       s01min  = 2*mass0*mass1;
       s12min  = 2*mass1*mass2;
       s02min  = 2*mass0*mass2;

       gDet = ((s01*s12*s02 - s01*s01*mass2*mass2 - s02*s02*mass1*mass1 - s12*s12*mass0*mass0)/4 + mass0*mass0*mass1*mass1*mass2*mass2);

       if gDet < 0.:
         print ("small gram determinant, discard emission")
         return False, [Vec4(),Vec4(),Vec4()]
         #sys.exit()

       # The r and R parameters in arXiv:1108.6172.
       rAntMap = ( sig2 + sqrt( sAnt*sAnt - sAntMin*sAntMin ) * ( s12-s12min - (s01-s01min) ) / ( s01-s01min + s12-s12min ) ) / (2*m2Ant);
       bigRantMap2 = 16*gDet * ( m2Ant*rAntMap * (1.-rAntMap) - (1.-rAntMap)*m2I - rAntMap*m2K ) + ( s02*s02 - s02min*s02min ) * ( sAnt*sAnt - sAntMin*sAntMin );

       bigRantMap = sqrt( bigRantMap2 );
       p1dotpI = (sig2*(s02*s02 - s02min*s02min) * (m2Ant + mass0*mass0 - mass1*mass1 - mass2*mass2 - s12) + 8*rAntMap*(m2Ant + mass0*mass0 - mass1*mass1 - mass2*mass2 - s12)*gDet - bigRantMap*(s02*s02 - s02min*s02min + s01*s02-2*s12*mass0*mass0)) / (4*(4*gDet + m2Ant*(s02*s02 - s02min*s02min)));

       apInum2 = m2Ant*m2Ant + m2I*m2I + m2K*m2K - 2*m2Ant*m2I - 2*m2Ant*m2K - 2*m2I*m2K;

       apI = sqrt(apInum2)/(2*mAnt)
       EI = sqrt( apI*apI + m2I )
       cosPsi = ((E0*EI) - p1dotpI)/(ap0*apI)

       if ge(cosPsi,1.):
         cosPsi=1.0
       elif le(cosPsi,-1.):
         cosPsi=-1.0

       sinPsi = sqrt(1. - cosPsi*cosPsi)

       phi = 2.*m.pi*r.random()
       cosPhi=cos(phi)
       sinPhi=sqrt(1.-cosPhi*cosPhi)

       #pi = Vec4(Ei,0.,0.,Ei)
       p1 = Vec4(p1cm.E,
         cosPsi * cosPhi * p1cm.px -  sinPhi * p1cm.py + sinPsi * cosPhi * p1cm.pz,
         cosPsi * sinPhi * p1cm.px +  cosPhi * p1cm.py + sinPsi * sinPhi * p1cm.pz,
         -sinPsi*          p1cm.px +  0.               + cosPsi          * p1cm.pz)

       #pk = Vec4(Ek, Ek*sin(Thetaik), 0., Ek*cosThetaik)
       p3 = Vec4(p3cm.E,
         cosPsi * cosPhi * p3cm.px -  sinPhi * p3cm.py + sinPsi * cosPhi * p3cm.pz,
         cosPsi * sinPhi * p3cm.px +  cosPhi * p3cm.py + sinPsi * sinPhi * p3cm.pz,
         -sinPsi         * p3cm.px +  0.               + cosPsi          * p3cm.pz)

       #pj = Vec4(Ej, -Ej*sin(Thetaij), 0., Ej*cosThetaij)
       p2 = Vec4(p2cm.E,
         cosPsi * cosPhi * p2cm.px -  sinPhi * p2cm.py + sinPsi * cosPhi * p2cm.pz,
         cosPsi * sinPhi * p2cm.px +  cosPhi * p2cm.py + sinPsi * sinPhi * p2cm.pz,
         -sinPsi         * p2cm.px +  0.               + cosPsi          * p2cm.pz)

       return True, [p1,p2,p3]

    def getNextSplitting(self,kappaMax):

        kappaMaxNow=-1e10
        tMax= -1
        depthmin=1000

        for t in range(len(self.baseline.tips)):
          tip=self.baseline.tips[t]

          if t in self.usedTips:
            continue

          if tip[0]==0:
            continue
          if tip[-1]<=2:
            continue

          depthmin=min(depthmin,tip[0])

          kappaNow=tip[3]
          if kappaNow>kappaMaxNow and kappaNow < kappaMax:
            kappaMaxNow=kappaNow
            tMax = t

        if ge(tMax,0.):
          tMaxList = []
          for t in range(len(self.baseline.tips)):

            if t in self.usedTips:
              continue

            tip=self.baseline.tips[t]

            if tip[0]==0:
              continue
            if tip[-1] <= 2:
              continue
            if tip[0] != depthmin:
              continue

            kappaNow=tip[3]
            if eq (kappaNow, kappaMaxNow):
              tMaxList.append(t)

          return True, tMaxList

        return False, []

    def updateEvent(self,event):

        # always put antiquark in last place in event
        for i in range(2,len(event)):
          for j in range(2,len(event)):
            if (event[j].pid < 0
               and event[j].pid > -10
               and event[i].pid > 0
               and (event[i].pid < 10 or event[i].pid == 21)):
                event[i], event[j] = event[j], event[i]

        # now construct a trivial color state.
        colindex=1
        for p in event:
          if p.pid > 0 and p.pid < 10:
            p.col = [colindex,0]
          if p.pid == 21:
            p.col = [colindex+1,colindex]
            colindex=colindex+1
          if p.pid < 0 and p.pid > -10:
            p.col = [0,colindex]

        return True
