
import math as m
import sys

from qcd import deltaYg
from basics import smearingfactor, eq, ne, le
from scales import LambdaQCD

stepsize=deltaYg/(smearingfactor+0.0)

class Baseline:

    def __init__(self,yMin,yMax):
        self.yMax=yMax
        self.yMin=yMin
        self.protoPath = []
        self.usedProtoPath = []
        self.usedNode = []
        self.path = []
        self.node = []

        self.iparticle = 2

        self.steps = []
        self.tips = []

        return None

    # Wrapper function to get list of all leaves (final folds w/o effective gluons)
    def createPath(self,info):

        # Create all the vectors 
        for i in info:
          depth=i[0]
          x1coord=i[1]
          x2coord=abs(i[2])
          direction=i[3]
          kappa=i[4]
          y0=i[5]
          yInterval=kappa

          # Draw primary plane's projection as lines of equal x2-coordinate (horizontal)
          if depth==0:
            self.protoPath.append( [depth,-0.5*yInterval,0.0,0.5*yInterval,0.0, -0.5*yInterval,0.0, 1, [kappa,y0]] )
          # Draw 2n-ary plane's projection as lines of equal x2-coordinate (horizontal)
          elif depth%2==0:
            # Positive rapidity side of fold
            self.protoPath.append( [depth,x1coord,x2coord,x1coord+ 0.5*direction*yInterval,x2coord,x1coord,x2coord, 1, [kappa,y0]])
            # Negative rapidity side of fold: swap xmin and xmax
            self.protoPath.append( [depth,x1coord+ 0.5*direction*yInterval,x2coord,x1coord,x2coord,x1coord,x2coord,-1, [kappa,y0]])
          # Draw (2n-1)-ary plane's projection as lines of equal x1-coordinate (vertical) 
          else:
            # Positive rapidity side of fold
            self.protoPath.append( [depth,x1coord,x2coord,x1coord,x2coord + 0.5*direction*yInterval,x1coord,x2coord, 1, [kappa,y0]])
            # Negative rapidity side of fold: swap ymin and ymax
            self.protoPath.append( [depth,x1coord,x2coord + 0.5*direction*yInterval,x1coord, x2coord,x1coord,x2coord, -1, [kappa,y0]])

        # order by depth
        self.protoPath=sorted(self.protoPath)

        # now go through from yMin -> yMax and create line segments, so that a vector always terminates when another vector intersects.
        n=m.floor(abs(self.protoPath[0][1])/stepsize)
        x1beg= -(n+1)*stepsize

        self.takeSteps(self.protoPath[0], x1beg, self.protoPath[0][2], self.protoPath[0][3], self.protoPath[0][4])

        # Step through the path in deltaYg-increments
        self.discretizePath()

        # now all but the last line segment are now attached. 

        # Attach the last line segment
        xend = self.yMin
        if len(self.path)>0:
          xend=self.path[-1][3]
        self.append(0, xend, 0., self.yMax, 0., self.yMax, 0., 1, 0.,0.)

        # now reset the particle index of the first and last tip:
        # Adjust the start 
        # of the first line segment to correct starting point, then append the
        # final segment.
        self.path[0][1] = self.yMin
        self.path[0][-1] = 1
        self.path[0][5] = 0.

        self.path[-1][-1] = 2

        #for pp in range(len(self.protoPath)):
        #  print ("proto ", pp, self.protoPath[pp])

        if len(self.path)==1 and len(self.protoPath)>1:
          for pp in range(len(self.protoPath)):
            print ("proto ", pp, self.protoPath[pp])
          print ("could not create path. Exit.")
          sys.exit()

        # Include the last few discrete steps as well.
        x1now = xend
        x1end = self.yMax
        x2=0.
        while x1now < x1end:
          x1nowNew, x2 = self.step( x1now, 0., x1end, 0.)
          self.steps.append([0,x1now,0.,x1nowNew,0.,0.,2])
          x1now = x1nowNew

        self.steps[0][-1] = 1
        self.steps[0][1] = self.yMin
        self.steps[-1][3] = self.yMax

        self.tips.append( [ 0, self.yMin,0.,1 ] )
        self.findTips()
        self.tips.append( [ 0, self.yMax,0.,2 ] )

        return True



    def append(self, depth, x1beg, x2beg, x1end, x2end, x1max, x2max, direction, kappa, y0):

        ipart=-1

        # store node 
        if ne(x1end,x1max) or ne(x2end,x2max):
          self.node.append( [ depth, x1end, x2end, x1max, x2max, direction] )
        else:
          self.iparticle=self.iparticle+1
          ipart=self.iparticle

        self.path.append( [ depth, x1beg, x2beg, x1end, x2end, [kappa,y0], direction, ipart])


###############################################################################

    def step(self, x1beg, x2beg, x1max, x2max):

        #print "in step: stepsize=", stepsize, smearingfactor

        if eq(x1beg, x1max):
          if abs(x2max-x2beg) < 1e-10:
            return x1beg, x2beg
          return x1beg, x2beg + (x2max-x2beg) / abs (x2max-x2beg) * stepsize
        elif eq(x2beg,x2max):
          if abs(x1max-x1beg) < 1e-10:
            return x1beg, x2beg
          return x1beg + (x1max-x1beg) / abs (x1max-x1beg) * stepsize, x2beg
        
        return x1beg, x2beg

###############################################################################
    def nextStart(self, pathNow, x1beg, x2beg, x1max, x2max):

        x1now, x2now = self.step( x1beg, x2beg, x1max, x2max)

        iP = []
        iN = []

        x1maxNow, x2maxNow = max(x1beg,x1max), max(x2beg,x2max)

        #print "In nextStart: x1 in [", x1beg, x1max, "]     x2 in [", x2beg, x2max, "], current point ", x1now, x2now

        # Loop through folds and nodes to get first candidates
        while le(x1now, x1maxNow) and le(x2now, x2maxNow):

          for i in range(len(self.protoPath)):

            x1=self.protoPath[i][1]
            x2=self.protoPath[i][2]

            if self.protoPath[i][0]<pathNow[0] or i in self.usedProtoPath:
              continue

            if eq(x1, x1now) and eq(x2, x2now):
              iP.append(i)

          for i in range(len(self.node)):
            if i in self.usedNode:
              continue
            x1=self.node[i][1]
            x2=self.node[i][2]
            if eq(x1, x1now) and eq(x2, x2now):
              iN.append(i)

          x1old, x2old = x1now, x2now
          x1now, x2now = self.step( x1now, x2now, x1max, x2max)

          #print "In nextStart: x1 in [", x1beg, x1max, "]     x2 in [", x2beg, x2max, "], current point ", x1now, x2now

          if eq(x1now,x1old) and eq(x2now,x2old):
            break

        #print "In nextStart: #paths=", len(iP), " #nodes=", len(iN)

        # Choose one of the possible candidates. General idea: Use the fold
        # that is closest to the nodal point, and keep track of direction
        # of the fold.
        # .. when moving along x1-direction
        istart=-1
        inode=-1
        mindiff=1e10
        if eq(x2beg,x2max):

          # keep only folds with good direction
          badPaths = []
          for i in iP:
            # when moving right in x1, prefer folds that branch out upward
            # --> bad paths branch out downward (x1beg>x2end)
            if x1beg<x1max and self.protoPath[i][2] > self.protoPath[i][4]:
              badPaths.append(i)
            # when moving left in x1, prefer folds that branch out downward
            # --> bad paths branch out upward (x2beg<x2end)
            if x1beg>x1max and self.protoPath[i][2] < self.protoPath[i][4]:
              badPaths.append(i)

            # if the path *is correctly moving* (rightward in x1, upward in x2)
            # then disallow paths with incorrect x2-anchor
            if x1beg<x1max and self.protoPath[i][2] < self.protoPath[i][4] and ne(self.protoPath[i][6], x2beg):
              badPaths.append(i)

            # if the path *is correctly moving* (leftward in x1, downward in x2)
            # then disallow paths with incorrect x2-anchor
            if x1beg>x1max and self.protoPath[i][2] > self.protoPath[i][4] and ne(self.protoPath[i][6],x2beg):
              badPaths.append(i)

            # if the *new* path is moving at fixed x2 (i.e. it's the "other side of the fold")
            # then disallow paths with incorrect x1-anchor
            if eq (self.protoPath[i][2], self.protoPath[i][4]) and ne(self.protoPath[i][5],x1beg):
              badPaths.append(i)

          # Use fold that is attached "closest" to original point
          newDirectionPaths = []
          sameDirectionPaths = []
          nPaths=0
          for i in iP:
            if i in badPaths:
              continue

            nPaths=nPaths+1
            if ne(self.protoPath[i][2],self.protoPath[i][4]):
              newDirectionPaths.append(i)
            else:
              sameDirectionPaths.append(i)

            if abs(self.protoPath[i][1] - x1beg) < mindiff:
              istart=i
              mindiff=abs(self.protoPath[i][1] -x1beg )

          # Check if nodes are available.
          mindiff=1e10
          for i in iN:
            if abs(self.node[i][1] - x1beg ) < mindiff:
              inode=i
              mindiff=abs(self.node[i][1] -x1beg ) 

          # Prefer nodes over paths in the same direction (paths in the same 
          # direction should *only* be used to jump over the tip of folds)
          if nPaths==len(sameDirectionPaths) and inode !=-1:
            istart=-1

          # If there are folds that branch out in new directions, prefer these.
          if len(newDirectionPaths) > 0:
            mindiff=1e10
            for i in iP:
              if i in badPaths:
                continue
              if i in sameDirectionPaths:
                continue
              if abs(self.protoPath[i][1] - x1beg) < mindiff:
                istart=i
                mindiff=abs(self.protoPath[i][1] -x1beg ) 

        # .. when moving along x2-direction
        elif eq(x1beg, x1max):

          # keep only folds with good direction
          badPaths = []
          for i in iP:
            # when moving upward in x2, prefer folds that branch out left
            # --> bad paths branch out to right (x1beg<x2end)
            if x2beg<x2max and self.protoPath[i][1] < self.protoPath[i][3]:
              badPaths.append(i)

            # when moving downward in x2, prefer folds that branch out right
            # --> bad paths branch out to left (x1beg>x2end)
            if x2beg>x2max and self.protoPath[i][1] > self.protoPath[i][3]:
              badPaths.append(i)

            # if the path *is correctly moving* (upward in x2, leftward in x1)
            # then disallow paths with incorrect x1-anchor
            if x2beg<x2max and self.protoPath[i][1] > self.protoPath[i][3] and ne(self.protoPath[i][5],x1beg):
              badPaths.append(i)

            # if the path *is correctly moving* (downward in x2, rightward in x1)
            # then disallow paths with incorrect x1-anchor
            if x2beg>x2max and self.protoPath[i][1] < self.protoPath[i][3] and ne(self.protoPath[i][5],x1beg):
              badPaths.append(i)

          # Use fold that is attached "closest" to original point
          for i in iP:
            if i in badPaths:
              continue
            if abs(self.protoPath[i][2] -x2beg ) < mindiff:
              istart=i
              mindiff=abs(self.protoPath[i][2] - x2beg ) 

          # Check if nodes are available
          mindiff=1e10
          for i in iN:
            if abs(self.node[i][2] - x2beg ) < mindiff:
              inode=i
              mindiff=abs(self.node[i][2] - x2beg )

        if istart!=-1:
          self.usedProtoPath.append(istart)
          return True, self.protoPath[istart]

        if inode!=-1:
          self.usedNode.append(inode)
          path = [self.node[inode][0],self.node[inode][1],self.node[inode][2],self.node[inode][3],self.node[inode][4],self.node[inode][1],self.node[inode][2] , self.node[inode][5], [0.,0.] ]
          return True, path


        return False, -1

###############################################################################
    def takeSteps(self, pathNow, x1beg, x2beg, x1max, x2max):

        if eq(x1beg, x1max) and eq(x2beg,x2max):
          print ("Error in Baseline:takeSteps: ", x1beg, x1max, x2beg, x2max)
          sys.exit()

        found, start = self.nextStart(pathNow, x1beg, x2beg, x1max, x2max)

        #print "In takeSteps: ", found, start

        if found:
          self.append(pathNow[0], x1beg, x2beg , start[1], start[2], x1max, x2max, pathNow[7], pathNow[8][0], pathNow[8][1])
          x1begNow = start[1]
          x2begNow = start[2]
          x1endNow = start[3]
          x2endNow = start[4]
          self.takeSteps(start, x1begNow, x2begNow, x1endNow, x2endNow)

        return True

    def findBegEnd(self,ibeg,iend):

        # find last instance of beginning
        ibegindex=-1
        for i in range(len(self.steps)):
          if self.steps[i][-1]==ibeg:
            ibegindex = i

        if ibegindex > 0:
          ibegindex=ibegindex+1

        # find first instance of end particle
        iendindex=-1
        for i in range(len(self.steps)-1,0,-1):
          if self.steps[i][-1]==iend:
            iendindex = i

        # special case for quark tips on baseline: use last index for end point.
        if iend==2:
          iendindex=-1
          for i in range(len(self.steps)-1,0,-1):
            if self.steps[i][-1]==iend:
              iendindex = i
              break

        if ibegindex<0 or iendindex<0:
          print ("Error in finding starting/stopping point for lambda calculation")
          sys.exit()

        iStepMin = ibegindex
        iStepMax= iendindex

        if iStepMin<0 or iStepMax <0:
          return -1, -1

        return iStepMin, iStepMax

###############################################################################

    def shortestDistance2(self, ibeg, iend):

        iStepMin, iStepMax = self.findBegEnd(ibeg,iend)

        nDoubleCounted = 0
        used = []
        for i in range(iStepMin,iStepMax+1):
          if i in used:
            continue
          for j in range(i,iStepMax+1):
            if j in used:
              continue
            if eq (self.steps[i][1], self.steps[j][3] ) and eq (self.steps[i][2], self.steps[j][4]) and eq (self.steps[i][3], self.steps[j][1]) and eq (self.steps[i][4], self.steps[j][2]) :
              used.append(i)
              used.append(j)
              nDoubleCounted=nDoubleCounted+2

        if iStepMin<0 or iStepMax <0:
          print ("Error in calculation of shortest distance for ", ibeg, iend,  " : ", iStepMin, iStepMax)
          return -1.0




        rap = (iStepMax-iStepMin+1)*stepsize - nDoubleCounted*stepsize

        # Correct for smaller size of steps at beginning/end of baseline
        if ibeg==1:
          size=m.sqrt( (self.steps[0][1]-self.steps[0][3])**2 + (self.steps[0][2]-self.steps[0][4])**2 )
          rap = rap + size - stepsize
        if iend==2:
          size=m.sqrt( (self.steps[-1][1]-self.steps[-1][3])**2 + (self.steps[-1][2]-self.steps[-1][4])**2 )
          rap = rap + size - stepsize

        #print ibeg, iend, iStepMin, iStepMax, nDoubleCounted, rap

        return rap

###############################################################################
    def discretizePath(self):

        for p in self.path:

          depth = p[0]
          x1now = p[1]
          x2now = p[2]
          x1end = p[3]
          x2end = p[4]
          kappa = p[5][0]

          if eq(x1now, x1end):
            while ne(x2now,x2end):
              x1nowNew, x2nowNew = self.step( x1now, x2now, x1end, x2end)
              self.steps.append([depth,x1now,x2now,x1nowNew,x2nowNew, kappa, p[-1]])
              x1now, x2now = x1nowNew, x2nowNew

          if eq(x2now, x2end):
            while ne(x1now,x1end):
              x1nowNew, x2nowNew = self.step( x1now, x2now, x1end, x2end)
              self.steps.append([depth,x1now,x2now,x1nowNew,x2nowNew, kappa, p[-1]])
              x1now, x2now = x1nowNew, x2nowNew

###############################################################################
    def findTips(self):

        part=self.iparticle

        for i in range(1,len(self.steps)):

          depth=self.steps[i][0]

          x1begNew = self.steps[i][1]
          x2begNew = self.steps[i][2]
          x1endNew = self.steps[i][3]
          x2endNew = self.steps[i][4]

          x1begOld = self.steps[i-1][1]
          x2begOld = self.steps[i-1][2]
          x1endOld = self.steps[i-1][3]
          x2endOld = self.steps[i-1][4]

          kappa = max(self.steps[i-1][5],self.steps[i][5])

          if eq (x1begNew, x1endNew) and eq (x1begOld, x1endOld) and eq (x2endNew, x2begOld) and eq (x2begNew, x2endOld) :

              self.tips.append([depth,x1endOld,x2endOld, kappa, part])
              self.tips.append([depth,x1endOld,x2endOld, kappa,-part])

              self.steps[i-1][-1] = part
              self.steps[i][-1]   =-part

              part=part+1

          elif eq (x2begNew, x2endNew) and eq (x2begOld, x2endOld) and eq (x1endNew, x1begOld) and eq (x1begNew, x1endOld) :
              self.tips.append([depth,x1endOld,x2endOld, kappa, part])
              self.tips.append([depth,x1endOld,x2endOld, kappa,-part])

              self.steps[i-1][-1] = part
              self.steps[i][-1]   =-part

              part=part+1

