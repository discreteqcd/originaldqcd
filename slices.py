
import math as m
from tiles import Tile
from qcd import deltaYg
from basics import eq, gt, lt, le

from random import randrange, random

class Slice:

    def __init__(self,yMin=0,yMax=0,yRef=0,kappaMin=0,kappaMax=0,kappaMaxMod=0):
        self.activeIndex = -1
        self.yMin = yMin
        self.yMax = yMax
        self.yCenter = 0.5*(self.yMax+self.yMin)
        self.yRef = yRef
        self.yRel = self.yCenter-self.yRef

        #print "create new slice at [", yMin, yMax , "]", " ycenter=", self.yCenter, " yRef=", yRef, " yRel=", self.yRel

        self.kappaMin = kappaMin
        self.kappaMax = kappaMax
        self.kappaMaxMod = kappaMaxMod

        self.tiles = []
        self.createTiles()

    def createTiles(self):
        kappaLowerEdge=self.kappaMin - deltaYg
        while kappaLowerEdge < self.kappaMax:
          kappaCenter = kappaLowerEdge + deltaYg
          if self.isInReducedTriangle(self.yCenter,kappaCenter):
            kappaUpperEdge = kappaLowerEdge + 2.*deltaYg

            area=0.0

            # Crude Monte-Carlo estimate of tile area.
            ntrials=100000
            kappaMinsave = self.kappaMin
            self.kappaMin = kappaLowerEdge
            for i in range(1,ntrials):
              ry = random()
              ynow = self.yMin + ry*deltaYg
              rk = random()
              know = kappaLowerEdge + rk*2.*deltaYg
              slope=-1
              if self.isInReducedTriangle(ynow,know):
                area=area+1.0
            area = area/(ntrials+0.0)*deltaYg*2.*deltaYg
            self.kappaMin = kappaMinsave

            #print " area of y in [", self.yMin, self.yMax, "], yRef=", self.yRef, " kappa in [",kappaLowerEdge, kappaUpperEdge, "] : crude area =", deltaYg*2.*deltaYg, " MC area=", area

            self.tiles.append(Tile( self.yMin, self.yMax, self.yRel, kappaLowerEdge, kappaUpperEdge, False, area))

          kappaLowerEdge=kappaLowerEdge + 2.*deltaYg

        #if len(self.tiles) == 0:
        #  print ("Warning: slice at yslice=[",self.yMin, self.yCenter, self.yMax,"] and kslice=[", self.kappaMin, self.kappaMax, "] and kMaxMod=",self.kappaMaxMod," contains no tiles.")
        #else:
        #  print ("Info: slice at yslice=[",self.yMin, self.yCenter, self.yMax,"] and kslice=[", self.kappaMin, self.kappaMax, "] and kMaxMod=",self.kappaMaxMod," contains ", len(self.tiles), " tiles.")

        return True

    def isInReducedTriangle(self,y,kappa):
        if gt(kappa,self.kappaMaxMod):
          return False
        if lt(kappa,self.kappaMin):
          return False
        if gt(y,self.yMax):
          return False
        if lt(y,self.yMin):
          return False
        return True

    def getActiveTile(self):
        return self.tiles[activeIndex]

    # Function to pick an active tile (=effective gluon)
    def pickActiveTile(self, useMaxKappa=False):
        if len(self.tiles)==0:
          return False
        if len(self.tiles)==1 and eq(self.tiles[-1].kappa,self.kappaMin):
          return False

        #use = len(self.tiles)==3 and eq(self.yCenter, self.yRef)
        #use = use or len(self.tiles)==1 and gt(self.yCenter, self.yRef)
        #if not use:
        #  return False
        #print "slice", len(self.tiles), self.yCenter, self.yRef

        activeIndexNow=-1
        if useMaxKappa:
          maxkappa=0.0
          maxindex=0        
          for i in range(len(self.tiles)):
            if self.tiles[i].kappa > maxkappa:
              maxkappa = self.tiles[i].kappa
              maxindex = i
          activeIndexNow = maxindex
        else:
          total_area=0.0
          for t in self.tiles:
            total_area=total_area + t.area

          prob_vec=[]
          for t in self.tiles:
            prob_vec.append(t.area/total_area)

          activeIndexNow = self.discrete_inverse_trans(prob_vec)

        #if len(self.tiles)==3 and eq(self.yCenter, self.yRef):
        #  activeIndexNow=2
        #if len(self.tiles)==2 and gt(self.yCenter, self.yRef):
         # activeIndexNow=1

        #for it in range(len(self.tiles)):
        #  print "tile", it, self.tiles[it].kappa

        #print activeIndexNow, self.tiles[activeIndexNow].kappaMin

          #activeIndexNow = randrange(len(self.tiles))
        if activeIndexNow > -1:
          activeIndex = activeIndexNow
          self.tiles[activeIndex].active = True
          return True
        return False


    def discrete_inverse_trans(self,prob_vec):
        U=random()
        if le(U,prob_vec[0]):
          return 0
        else:
          for i in range(1,len(prob_vec)+1):
              if sum(prob_vec[0:i])<U and sum(prob_vec[0:i+1])>U:
                  return i
