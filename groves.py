
import math as m
import random as r

from folds import Fold

import sys

from qcd import deltaYg, deltaYq
from basics import smearingfactor, eq, ne

class Grove:

    # overall idea: construct the "grove" of "trees" recursively:
    # 0) Set depth=0 and create the background/primary Lund plane/fold
    # 1) Pick & create all effective gluons on the current fold
    # 2) Set depthNow=depth, loop through effective gluons (moving from high to low kappa)
    # 3) For each effective gluon, create a new fold by calling the constructor, starting from point 1) with depth->depth+1.
    # 4) Store new folds in an array of "children" of fold of "depth" depthNow
    def __init__(self,depth=0,kappaMin=0.0,kappaMax=0.0,y0=0.0,yMin=0.0,yMax=0.0,x1coord=0.0,x2coord=0.0,direction=0.0,useMaxKappa=False):
        # Store inputs
        self.depth = depth
        #print ("new grove with depth", depth)
        self.kappaMin = kappaMin
        self.kappaMax = kappaMax
        self.y0 = y0
        self.yMin = yMin
        self.yMax = yMax

        self.x1coord = x1coord
        self.x2coord = x2coord
        self.direction =  direction

        self.kappaMaxTile = m.floor((self.kappaMax)/(2.*deltaYg)) * 2.*deltaYg

        self.useMaxKappa=useMaxKappa
        # Create new fold
        self.fold = []
        self.fold.append(Fold(self.depth,self.kappaMin,self.kappaMax,self.y0,
          self.yMin,self.yMax,self.x1coord,self.x2coord,self.direction))
        if len(self.fold[-1].slices)==0:
          self.fold.pop()

        # Pick effective gluons and create new folds. This will create the 
        # array of children recursively using this very initializer
        if len(self.fold)>0:
          self.pickEffectiveGluons(-1)
        self.children = []
        if len(self.fold)>0:
          self.createEffectiveGluonFolds(-1)

        self.effectiveGluons = []

        #if len(self.fold)>0:
        #  self.fold[-1].nTiles()
        #  self.fold[-1].printTile()

    # Function to pick all effective gluons on the fold.
    # Strategy: loop through all slices in the fold, for each slice, find 
    # active tile (=effective gluon position)
    def pickEffectiveGluons(self,foldindex=-1):
        self.fold[foldindex].pickEffectiveGluons(self.useMaxKappa)
        return True

    def doSmearing(self,kappa):
        if self.depth==0 and eq(self.kappaMaxTile, kappa):
          return True
        nextTokappaMaxTile = (m.floor((self.kappaMax)/(2.*deltaYg))-1) * 2.*deltaYg
        if self.depth==0 and eq(nextTokappaMaxTile, kappa):
          return True
        return False

    def smearedKinematics(self,y0,kappa, glueindex, effectiveGluons):
        delta=deltaYg
        if self.depth==0:
          delta=deltaYq

        y0old=y0
        kappaold=kappa

        isNotHighestTile = self.fold[-1].hasTileAt(y0-0.5*deltaYg,y0+0.5*deltaYg,kappa+2.*deltaYg)

        yrange=1
        #if eq (y0,0.0):
        #  yrange=2

        yMinNow = y0-0.5*yrange*deltaYg
        kappaMinNow=kappa-deltaYg;
        kappaMaxNow=kappa+deltaYg;
        found=False
        counter=0
        while not found and counter < 10000000:

          counter=counter+1

          yint = r.randrange(0,yrange*smearingfactor+1,1)
          ynow = yMinNow + yint/(yrange*smearingfactor+0.0)*yrange*deltaYg

          deltaymin=1e10
          ynext=1e10
          badY=False
          for effectiveGluon in effectiveGluons:
            ytest=effectiveGluon[1]
            if eq (ytest, y0):
              continue
            deltayminNow=min(deltaymin, abs(y0-ytest))
            if deltayminNow<deltaymin:
              deltaymin=deltayminNow
              ynext=ytest

            if abs(ynow-ynext)<1e-5:
              badY=True

          if badY:
            print ("bad y -> continue")
            continue

          # Find highest kappa value for this tile.
          yMaxNow=yMinNow+deltaYg
          if yMinNow < self.y0:
            slope=1
          if yMinNow > self.y0:
            slope=-1
          kappaMaxModMin = slope*(yMinNow-self.y0)*(self.kappaMin-self.kappaMax)/(self.yMin-self.y0) + self.kappaMax - 2.*delta/2.;
          if yMaxNow < self.y0:
            slope=1
          if yMaxNow > self.y0:
            slope=-1
          kappaMaxModMax = slope*(yMaxNow-self.y0)*(self.kappaMin-self.kappaMax)/(self.yMin-self.y0) + self.kappaMax - 2.*delta/2.;
          if y0 < self.y0:
            slope=1
          if y0 > self.y0:
            slope=-1
          kappaMaxModCen = slope*(y0-self.y0)*(self.kappaMin-self.kappaMax)/(self.yMin-self.y0) + self.kappaMax - 2.*delta/2.;

          kappaMaxMod=max(kappaMaxModMin,max(kappaMaxModCen,kappaMaxModMax))

          prefac=2
          #while ne (y0, 0.0) and kappaMaxMod>kappaMinNow+prefac*deltaYg:
          #  prefac=prefac+1

          if isNotHighestTile:
            prefac=2

          kappasmearing=smearingfactor
          if prefac>2:
            kappasmearing= prefac*smearingfactor/2

          #print "In smearedKinematics: ", smearingfactor, prefac, kappasmearing
          #print y0, " kappa range = [", kappaMinNow, kappaMinNow + prefac*deltaYg, "]", prefac, kappa, kappaMaxMod

          kint = r.randrange(0,kappasmearing+1,1)
          know = kappaMinNow+kint/(kappasmearing+0.0)*prefac*deltaYg
          slope=-1
          if ynow < self.y0:
            slope=1
          kappaMaxModSlice = slope*(ynow-self.y0)*(self.kappaMin-self.kappaMax)/(self.yMin-self.y0) + self.kappaMax - 2.*delta/2.;

          if kappaMaxModSlice > know:
            found=True
            y0    = ynow
            kappa = know
            break

        effectiveGluons[glueindex][0] = kappa
        effectiveGluons[glueindex][1] = y0
        effectiveGluons[glueindex][2] = effectiveGluons[glueindex][2] - y0old + y0

        return y0, kappa

    # Function to create all effective gluon folds
    def createEffectiveGluonFolds(self,foldindex=-1):
        # Get all effective gluon positions in the fold.
        # effectiveGluons is a list of tuples of format (kappaGluon,yGluon)
        effectiveGluons = self.fold[foldindex].getEffectiveGluonPositions()

        for g in range(len(effectiveGluons)):

               effectiveGluon = effectiveGluons[g]

               # Extract/calculate the dimensions of the new fold.
               depth=self.depth+1
               kappaMin=self.kappaMin
               kappaMax=effectiveGluon[0]
               y0=effectiveGluon[1]
               yRel = effectiveGluon[2]
               y0old=y0
               kappaOld=kappaMax

               if self.doSmearing(kappaMax):
                 y0old = y0
                 y0, kappaMax = self.smearedKinematics(y0,kappaMax, g, effectiveGluons)
                 yRel  = yRel - y0old + y0

               yMin = y0 - kappaMax/2.
               yMax = y0 + kappaMax/2.
               direction = 0
               if round(y0,3) > 0 and round(yRel,3) > 0:
                 direction = -1
               elif round(y0,3) > 0 and round(yRel,3) < 0:
                 direction = 1
               elif round(y0,3) < 0 and round(yRel,3) > 0:
                 direction = -1
               elif round(y0,3) < 0 and round(yRel,3) < 0:
                 direction = 1
               elif eq (y0,0.0) and round(yRel,3) > 0:
                 direction = -1
               elif eq (y0, 0.0) and round(yRel,3) < 0:
                 direction = 1

               # draw all first folds upward
               if depth==1:
                 direction = 1

               x1coord=self.x1coord
               x2coord=self.x2coord
               if depth%2==0:
                 x2coord+=yRel
               else:
                 x1coord+=yRel

               if kappaMax > kappaMin and abs(yMax-yMin) > deltaYg:  
                 # Now recurse (current function is called from constructor)
                 self.children.append( Grove(depth, kappaMin, kappaMax, y0,
                   yMin, yMax, x1coord,x2coord,direction,
                   self.useMaxKappa) )
                 if len(self.children[-1].fold)==0:
                   self.children.pop()

        return True

    # Function to calculate the rapidity distances of folds after the evolution
    def getFoldInformation(self):
        info = []

        self.collectFolds(info)

        return info

    # Recursively get all leaves (=folds with no folds attached to them)
    def collectFolds(self,info):

        for i in range(len(self.fold)):
          info.append((self.depth,
            self.fold[i].x1coord,
            self.fold[i].x2coord,
            self.fold[i].direction,
            self.fold[i].kappaMax,
            self.fold[i].y0))

        for i in range(len(self.children)):
          self.children[i].collectFolds(info)

        return True


