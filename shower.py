# system imports
from __future__ import print_function
import math as m
from math import log as ln
import sys, optparse

sys.path.append('utils')
from matrix import eetojj
from LHEF3 import *

# basic grove data structure
from groves import Grove
from kinematics import Kinematics
from baseline import Baseline
from scales import LambdaQCD, LambdaDiscrete, LambdaCutOff
from qcd import deltaYg

# inputs for grove creation
useMaxKappa=False
ecm=91.2

# Calculate properties of phase space triangle
kappaMin=ln(LambdaCutOff*LambdaCutOff/(LambdaQCD*LambdaQCD))
kappaMax=ln(ecm*ecm/(LambdaQCD*LambdaQCD))
yMin=-kappaMax/2.
yMax=kappaMax/2.
y0=0.0

# init event generation
hardprocess = eetojj(ecm)

# LHEF Initialization
lhe = LHEF("dqcd.lhe")
initrwgt = LHAinitrwgt()
wt = LHAweight()
wt.id = "central"
wt.text = " mur=" + str(0.5*ecm) + " muf=" + str(0.5*ecm)
initrwgt.weights[wt.id] = wt
lhe.SetHeader(3.0,initrwgt)

init = LHAinit()
init.beams = [11,-11]
init.energy = [0.5*ecm,0.5*ecm]
init.pdfgroup = [-1,-1]
init.pdfset = [-1,-1]
init.wgtid = -4
xsec = 0
xerr = 0

nevents=10000
for iev in range(0,nevents):

  print ("\n\n|------new event: #", iev,"-------------|")

  grove = Grove(0, kappaMin, kappaMax, y0, yMin, yMax, 0.0,0.0,0,useMaxKappa)
  baseline = Baseline(yMin,yMax)

  event, weight = hardprocess.GeneratePoint()

  if event[2].mom.Y() < 0.:
    event[2].SetPos(1)
    event[3].SetPos(2)
  else:
    event[2].SetPos(2)
    event[3].SetPos(1)

  xsec += weight
  xerr += weight*weight

  baseline.createPath(grove.getFoldInformation()) 
  kinematics = Kinematics(baseline)
  kinematics.createEvolvedEvent(event,2.*yMax)
  lhe.CreateEvent(event,weight)

process = LHAProcess(xsec,xerr,xsec,9999)
init.processes.append(process)
lhe.SetInitBlock(init)
lhe.AddAllEvents()
lhe.Write()

