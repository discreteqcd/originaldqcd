
import math as m
from slices import Slice
from qcd import deltaYg, deltaYq
from basics import eq, lt, gt, ne

class Fold:

    def __init__(self,depth=0,kappaMin=0,kappaMax=0,y0=0,yMin=0,yMax=0,x1coord=0,x2coord=0,direction=0):
        self.depth = depth
        self.kappaMin = kappaMin
        self.kappaMax = kappaMax
        self.y0 = y0
        self.yMin = yMin
        self.yMax = yMax

        #print "\n\nCreate new fold of depth", self.depth, "at [", yMin, y0, yMax , "]"

        self.slices = []
        self.createSlices()

        #print "Fold of depth ", self.depth, " has ", len(self.slices), " rapidity slices."
        #for s in self.slices:
        #  print "  yslice [", s.yMin, s.yMax, "] contains ", len(s.tiles) , " tiles "
        #  for t in s.tiles:
        #    print "    ytile [", t.yMin, t.yMax, "]  ktile=[", t.kappaMin, t.kappaMax,"]  area=", t.area

        self.x1coord = x1coord
        self.x2coord = x2coord
        self.direction =  direction

    def nTiles(self):
        ntiles=0
        for s in self.slices:
          if self.depth!=0 and eq(s.yCenter, self.y0):
            continue
          ntiles=ntiles+len(s.tiles)
        print ("depth=",self.depth, "ntiles=", ntiles)
        return

    def printTile(self):
        for s in self.slices:
          for t in s.tiles:
            print ("tile at ", t.yMin, t.yMax, t.kappa)

    def hasTileAt(self,yMin,yMax,kappa):
        for s in self.slices:
          for t in s.tiles:
            if eq(t.yMin,yMin) and eq(t.yMax,yMax) and eq(t.kappa,kappa):
              return True
        return False

    def createSlices(self):
        yUpperEdge=self.y0 + 0.5*deltaYg
        yLowerEdge=self.y0 - 0.5*deltaYg

        # Allowed phase space gets modified by removing large-rapidity 
        # regions ~ y< ymin+deltaYg/2 and y>ymax-deltaYg/2

        delta=deltaYg
        if self.depth==0:
          delta=deltaYq

        #print ("")
        #print ("")
        #print ("Info: create slices for depth", self.depth)

        kappaMaxModSlice = self.kappaMax - 2.*delta/2.
        # Create central slice.
        #print " new cen slice for ", self.y0, " at y= [", yLowerEdge,yUpperEdge,"]"
        self.slices.append(Slice( yLowerEdge,yUpperEdge,self.y0, self.kappaMin,self.kappaMax,kappaMaxModSlice))

        while yUpperEdge < self.yMax and yLowerEdge > self.yMin:

          # Append negative rapodity slice
          yCenter = yLowerEdge - 0.5*deltaYg;
          # Same boundary functions, but now with positive slope
          kappaMaxSlice = (yCenter-self.y0)*(self.kappaMin-self.kappaMax)/(self.yMin-self.y0) + self.kappaMax;
          kappaMaxModSlice = (yCenter-self.y0)*(self.kappaMin-self.kappaMax)/(self.yMin-self.y0) + self.kappaMax - 2.*delta/2.;

          if gt(kappaMaxSlice,self.kappaMin):
            #print " new neg slice for ", self.y0, " at y= [", yLowerEdge-deltaYg,yLowerEdge,"]", kappaMaxModSlice
            self.slices.append(Slice( yLowerEdge-deltaYg,yLowerEdge, self.y0, self.kappaMin,kappaMaxSlice,kappaMaxModSlice))

          # Update and continue to next slice.
          yUpperEdge=yUpperEdge + deltaYg
          yLowerEdge=yLowerEdge - deltaYg

          # Append "positive" rapidity slice
          yCenter = yUpperEdge - 0.5*deltaYg;
          # Maximal kappa value determined from boundary of phase space triangle
          kappaMaxSlice = -(yCenter-self.y0)*(self.kappaMin-self.kappaMax)/(self.yMin-self.y0) + self.kappaMax;
          # Allowed phase space gets modified by removing large-rapidity regions ~ y< ymin+deltaYg/2 and y>ymax-deltaYg/2
          kappaMaxModSlice = -(yCenter-self.y0)*(self.kappaMin-self.kappaMax)/(self.yMin-self.y0) + self.kappaMax - 2.*delta/2.;
          if gt(kappaMaxSlice,self.kappaMin):
            #print " new pos slice for ", self.y0, " at y= [", yUpperEdge-deltaYg,yUpperEdge,"]", kappaMaxModSlice
            self.slices.append(Slice( yUpperEdge-deltaYg,yUpperEdge,self.y0, self.kappaMin,kappaMaxSlice,kappaMaxModSlice))

        return True

    # Function to pick all effective gluons on the fold.
    # Strategy: loop through all slices in the fold, for each slice, find 
    # active tile (=effective gluon position)
    def pickEffectiveGluons(self,useMaxKappa=False):
        for i in range(len(self.slices)):
          # Central slice only active in background triangle. For all folds, a
          # central slice would be less than deltaYg away from emissions in the
          # parent fold, i.e. there are no effective gluons in the central slices.

          #print "set active ", self.depth, self.y0, self.slices[i].yCenter, self.slices[i].yRel 

          if self.depth>0 and eq(self.y0, self.slices[i].yCenter):
          #  print "skip slice ", self.depth, self.slices[i].yCenter
            continue

          #use = self.depth==0 and eq(0.0, self.slices[i].yCenter)
          #use = use or self.depth==1
          #if not use:
          #  continue

          #print "set active ", self.depth, self.y0, self.slices[i].yCenter, self.slices[i].yRel, len(self.slices[i].tiles)

          self.slices[i].pickActiveTile(useMaxKappa)
        return True

    # Function to pick all effective gluons on the fold.
    # Strategy: loop through all slices in the fold, for each slice, find 
    # active tile (=effective gluon position)
    def getEffectiveGluonPositions(self):
        positions = []
        for iS in range(len(self.slices)):

          #use = (self.depth==0 and eq(self.slices[iS].yCenter,0.))
          ##use = use or (self.depth==1 and gt(self.slices[iS].yCenter,0.))
          #use = use or (self.depth==1)
          #if not use: 
          #  continue

          for iT in range(len(self.slices[iS].tiles)):

            #if self.depth==0 and ne(self.slices[iS].tiles[iT].kappa, 4.*deltaYg):
            #  continue

            #if self.depth==1 and ne(self.slices[iS].tiles[iT].kappa, 2.*deltaYg):
            #  continue

            if self.slices[iS].tiles[iT].active == True:
              positions.append([self.slices[iS].tiles[iT].kappa,
                self.slices[iS].tiles[iT].y, self.slices[iS].tiles[iT].yRel,
                self.depth])
        return positions

