
import math as m

class Tile:

    def __init__(self,yMin=0,yMax=0,yRel=0,kappaMin=0,kappaMax=0,active=False, area=0.0):
        #print ("new tile with kappaMax", kappaMax)
        self.yMin     = yMin
        self.yMax     = yMax
        self.kappaMin = kappaMin
        self.kappaMax = kappaMax
        self.kappa    = 0.5*(kappaMax+kappaMin)
        self.y        = 0.5*(yMax+yMin)
        self.yRel     = yRel
        self.active   = active
        self.area     = area

