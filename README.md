# Discrete QCD: a Python implementation of discretized particle evolution

Welcome to the repository of the Discrete QCD parton shower. This is a completely new implementation of the [original algorithm](https://inspirehep.net/literature/404514), which was lost to time.

## Requirements 

The code is written in Python 3.

## Running the code.

You can run the code my executing the ``shower.py`` script.

## Authors and acknowledgment

The implementation is due to Stefan Prestel -- feel free to get in touch! Very big thanks to Gösta Gustafson for feedback and advice.

## License

This software is licenced under the GNU GPL v2 or later.

